


<div id="top"></div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#swagger-starter-and-ui">Swagger Starter and UI</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project


Blog app to be continued...

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This section should list any major frameworks/libraries used to bootstrap your project. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.

* springboot
* springboot web
* Spring data jpa
* lombok
* springboot devtools
* h2 database
* gradle


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites
* java 11
* springboot 2.6.0



### Installation

...

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage
#### Pagination Support
* content
* pageNo
* pageSize
* totalElements
* totalPages
* last



<p align="right">(<a href="#top">back to top</a>)</p>



<!-- Swagger -->
## Swagger Starter and UI
```
# swagger starter
http://localhost:8080/v3/api-docs

# swagger ui
http://localhost:8080/swagger-ui/
```
....
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->
## Roadmap
....
<p align="right">(<a href="#top">back to top</a>)</p>






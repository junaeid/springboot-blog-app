package com.springboot.blog.app.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.springboot.blog.app.models.Comment;
import com.springboot.blog.app.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest
public class CommentRepositoryTests {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;

    private Comment comment;
    private Post post;

    @BeforeEach
    public void setup() {
        post = Post.builder()
                .title("A simple Post")
                .content("why we should do unit testing")
                .description("This post is about testcase")
                .build();
        Post savedPost = postRepository.save(post);
        comment = Comment.builder()
                .name("username")
                .email("user@gmail.com")
                .body("comment body")
                .post(postRepository.findById(1L).orElse(null))
                .build();
    }

    @DisplayName("Junit test for save comment")
    @Test
    public void givenCommentObject_whenSave_thenReturnSavedCommentObject() {
        // given - precondition or setup

        // when - action or behavior that we are going to test
        Comment savedComment = commentRepository.save(comment);

        // then - verify the output
        assertThat(savedComment).isNotNull();
        assertThat(savedComment.getId()).isGreaterThan(0);
    }

    @DisplayName("Junit test for  get all comments based on Id")
    @Test
    public void givenCommentsObject_whenFindAllByPostId_thenComments() {
        // given - precondition or setup
        Comment comment1 = Comment.builder()
                .name("username2")
                .email("user2@gmail.com")
                .body("comment body 2")
                .post(postRepository.findById(1L).orElse(null))
                .build();

        commentRepository.save(comment);
        commentRepository.save(comment1);

        // when - action or behavior that we are going to test
        List<Comment> comments = commentRepository.findByPostId(1L);

        // then - verify the output
        assertThat(comments).isNotNull();
        assertThat(comments.size()).isEqualTo(2);
    }



}

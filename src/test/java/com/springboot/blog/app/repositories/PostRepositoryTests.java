package com.springboot.blog.app.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.springboot.blog.app.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest
public class PostRepositoryTests {

    @Autowired
    private PostRepository postRepository;

    private Post post;

    @BeforeEach
    public void setup() {
        post = Post.builder()
                .title("A simple Post")
                .content("why we should do unit testing")
                .description("This post is about testcase")
                .build();
    }

    @DisplayName("Junit test for save post operation ")
    @Test
    public void givenPostObject_whenSave_thenReturnSavedPostObject() {
        // given - precondition or setup

        // when - action or behavior that we are going to test
        Post savedPost = postRepository.save(post);

        // then - verify the output
        assertThat(savedPost).isNotNull();
        assertThat(savedPost.getId()).isGreaterThan(0);
    }

    @DisplayName("Junit test for get all Post Operation")
    @Test
    public void givenPostList_whenFindAll_thenPostList() {
        // given - precondition or setup
        Post post1 = Post.builder()
                .title("A simple Post again")
                .content("why we should do unit testing")
                .description("This post is about testcase")
                .build();

        postRepository.save(post);
        postRepository.save(post1);

        // when - action or behavior that we are going to test
        List<Post> posts = postRepository.findAll();

        // then - verify the output
        assertThat(posts).isNotNull();
        assertThat(posts.size()).isEqualTo(2);
    }

    @DisplayName("Junit test for  post by Id operation")
    @Test
    public void givenPostObject_whenFindById_thenReturnPostById() {
        // given - precondition or setup
        postRepository.save(post);

        // when - action or behavior that we are going to test
        Post savedPost = postRepository.findById(post.getId()).orElse(null);

        // then - verify the output
        assertThat(savedPost).isNotNull();
        assertThat(savedPost.getId()).isEqualTo(post.getId());
    }

    @DisplayName("Junit test for update post by Id")
    @Test
    public void givenPostObject_whenUpDateById_thenReturnUpdatedPost() {
        // given - precondition or setup
        postRepository.save(post);

        // when - action or behavior that we are going to test
        Post savedPost = postRepository.findById(post.getId()).orElse(null);
        assert savedPost != null;
        savedPost.setTitle("updated title");
        savedPost.setContent("updated content");
        savedPost.setDescription("updated description");
        Post updatedPost = postRepository.save(savedPost);

        // then - verify the output
        assertThat(updatedPost.getTitle()).isEqualTo("updated title");
        assertThat(updatedPost.getContent()).isEqualTo("updated content");
        assertThat(updatedPost.getDescription()).isEqualTo("updated description");
    }
}

package com.springboot.blog.app.services;

import com.springboot.blog.app.models.Post;
import com.springboot.blog.app.repositories.PostRepository;
import com.springboot.blog.app.services.impl.PostServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PostServiceTests {

    @Mock
    private PostRepository postRepository;
    @InjectMocks
    private PostService postService;

    private Post post;

    @BeforeEach
    public void setup(){
        post = Post.builder()
                .title("A simple Post")
                .content("why we should do unit testing")
                .description("This post is about testcase")
                .build();
    }
}

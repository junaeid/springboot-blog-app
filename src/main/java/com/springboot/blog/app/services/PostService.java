package com.springboot.blog.app.services;

import com.springboot.blog.app.dtos.PostDto;
import com.springboot.blog.app.dtos.PostResponse;

public interface PostService {
    PostDto createPost(PostDto postDto);

    PostResponse getAllPost(int pageNo, int pageSize, String sortBy, String sortDir);

    PostDto getPostById(Long id);

    PostDto updatePost(PostDto postDto, Long id);

    void deletePostById(Long id);
}

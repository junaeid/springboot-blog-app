package com.springboot.blog.app.services.impl;

import com.springboot.blog.app.dtos.CommentDto;
import com.springboot.blog.app.exceptions.BlogAPIException;
import com.springboot.blog.app.exceptions.ResourceNotFoundException;
import com.springboot.blog.app.models.Comment;
import com.springboot.blog.app.models.Post;
import com.springboot.blog.app.repositories.CommentRepository;
import com.springboot.blog.app.repositories.PostRepository;
import com.springboot.blog.app.services.CommentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public CommentServiceImpl(
            CommentRepository commentRepository,
            PostRepository postRepository,
            ModelMapper modelMapper
    ) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public List<CommentDto> getCommentsByPostId(Long postId) {
        //retrieve comments by postId
        List<Comment> comments = commentRepository.findByPostId(postId);

        return comments.stream().map(this::mapToDTO).collect(Collectors.toList());
    }

    @Override
    public CommentDto getCommentById(Long postId, Long commentId) {
        Comment comment = getCommentByCommentIdAndPostIdWithException(postId, commentId);
        return mapToDTO(comment);
    }

    @Override
    public CommentDto createComment(Long postId, CommentDto commentDto) {
        Comment comment = mapToDomain(commentDto);

        //retrieve post entity by id
        Post post = getPostById(postId);

        // set post to comment domain
        comment.setPost(post);

        //comment domain to db
        Comment newComment = commentRepository.save(comment);

        //mapped to dto and return
        return mapToDTO(newComment);
    }

    @Override
    public CommentDto updateComment(Long postId, Long commentId, CommentDto commentRequest) {

        Comment comment = getCommentByCommentIdAndPostIdWithException(postId, commentId);

        comment.setName(commentRequest.getName());
        comment.setEmail(commentRequest.getEmail());
        comment.setBody(commentRequest.getBody());

        Comment updatedComment = commentRepository.save(comment);


        return mapToDTO(updatedComment);
    }


    @Override
    public void deleteCommentById(Long postId, Long commentId) {

        Comment comment = getCommentByCommentIdAndPostIdWithException(postId, commentId);

        commentRepository.delete(comment);
    }

    private Comment getCommentByCommentIdAndPostIdWithException(Long postId, Long commentId) {
        //retrieve post entity by id
        Post post = getPostById(postId);

        // retrieve comment by Id
        Comment comment = getCommentById(commentId);

        if (!comment.getPost().getId().equals(post.getId())) {
            throw new BlogAPIException(HttpStatus.BAD_REQUEST, "Comment does not belong to the post");
        }
        return comment;
    }

    public Post getPostById(Long postId) {
        return postRepository
                .findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "Post",
                        "id",
                        postId
                ));
    }

    public Comment getCommentById(Long commentId) {
        return commentRepository.findById(commentId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "Comment",
                        "Id",
                        commentId
                ));
    }

    private CommentDto mapToDTO(Comment comment) {
        CommentDto commentDto = modelMapper.map(comment, CommentDto.class);

        /*CommentDto commentDto = new CommentDto();

        commentDto.setId(comment.getId());
        commentDto.setName(comment.getName());
        commentDto.setEmail(comment.getEmail());
        commentDto.setBody(comment.getBody());
        */
        return commentDto;
    }

    private Comment mapToDomain(CommentDto commentDto) {
        Comment comment = modelMapper.map(commentDto, Comment.class);

        /*Comment comment = new Comment();

        comment.setId(commentDto.getId());
        comment.setName(commentDto.getName());
        comment.setEmail(commentDto.getEmail());
        comment.setBody(commentDto.getBody());
        */
        return comment;
    }


}

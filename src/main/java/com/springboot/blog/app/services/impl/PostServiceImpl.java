package com.springboot.blog.app.services.impl;

import com.springboot.blog.app.dtos.PostDto;
import com.springboot.blog.app.dtos.PostResponse;
import com.springboot.blog.app.exceptions.ResourceNotFoundException;
import com.springboot.blog.app.models.Post;
import com.springboot.blog.app.repositories.PostRepository;
import com.springboot.blog.app.services.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    private final ModelMapper modelMapper;

    public PostServiceImpl(
            PostRepository postRepository,
            ModelMapper modelMapper
    ) {
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public PostDto createPost(PostDto postDto) {
        //map dto to domain
        Post post = mapToDomain(postDto);

        Post newPost = postRepository.save(post);

        //map domain to DTO and return the response
        return mapToDTO(newPost);
    }


    @Override
    public PostResponse getAllPost(int pageNo, int pageSize, String sortBy, String sortDir) {

        Sort sort = sortDir.
                equalsIgnoreCase(Sort.Direction.ASC.name()) ?
                Sort.by(sortBy).ascending() :
                Sort.by(sortBy).descending();

        //create a pageable instance
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);

        Page<Post> posts = postRepository.findAll(pageable);

        //get content for page object
        List<Post> postList = posts.getContent();
        List<PostDto> content = postList
                .stream()
                .map(this::mapToDTO)
                .collect(Collectors.toList());

        PostResponse postResponse = new PostResponse();

        postResponse.setContent(content);
        postResponse.setPageNo(posts.getNumber());
        postResponse.setPageSize(posts.getSize());
        postResponse.setTotalElements(posts.getTotalElements());
        postResponse.setTotalPages(posts.getTotalPages());
        postResponse.setLast(posts.isLast());

        return postResponse;

    }

    @Override
    public PostDto getPostById(Long id) {
        Post post = postRepository.
                findById(id).
                orElseThrow(() -> new ResourceNotFoundException(
                        "Post",
                        "id",
                        id
                ));
        return mapToDTO(post);
    }

    @Override
    public PostDto updatePost(PostDto postDto, Long id) {
        //get post by id from the database
        Post post = postRepository.
                findById(id).
                orElseThrow(() -> new ResourceNotFoundException(
                        "Post",
                        "id",
                        id
                ));

        post.setTitle(postDto.getTitle());
        post.setDescription(postDto.getDescription());
        post.setContent(postDto.getContent());

        Post updatePost = postRepository.save(post);

        return mapToDTO(updatePost);
    }

    @Override
    public void deletePostById(Long id) {
        //get post by id from the database
        Post post = postRepository.
                findById(id).
                orElseThrow(() -> new ResourceNotFoundException(
                        "Post",
                        "id",
                        id
                ));
        postRepository.delete(post);
    }

    //convert dto to domain
    private Post mapToDomain(PostDto postDto) {
        Post post = modelMapper.map(postDto, Post.class);
        /*
        Post post = new Post();

        post.setTitle(postDto.getTitle());
        post.setDescription(postDto.getDescription());
        post.setContent(postDto.getContent());
        */
        return post;
    }

    // convert domain into dto
    private PostDto mapToDTO(Post post) {
        PostDto postDto = modelMapper.map(post, PostDto.class);

        /*
        PostDto postDto = new PostDto();

        postDto.setId(post.getId());
        postDto.setTitle(post.getTitle());
        postDto.setDescription(post.getDescription());
        postDto.setContent(post.getContent());
        */
        return postDto;
    }
}

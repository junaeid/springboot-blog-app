package com.springboot.blog.app.services;

import com.springboot.blog.app.dtos.CommentDto;

import java.util.List;


public interface CommentService {
    List<CommentDto> getCommentsByPostId(Long postId);

    CommentDto getCommentById(Long postId, Long commentId);

    CommentDto createComment(Long postId, CommentDto commentDto);

    CommentDto updateComment(Long postId, Long commentId, CommentDto commentRequest);

    void deleteCommentById(Long postId, Long commentId);
}

package com.springboot.blog.app.repositories;

import com.springboot.blog.app.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {

}

package com.springboot.blog.app.controllers;

import com.springboot.blog.app.dtos.CommentDto;
import com.springboot.blog.app.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/posts/{postId}/comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }


    @GetMapping
    public List<CommentDto> getAllCommentsByPostId(
            @PathVariable("postId") Long postId
    ) {
        return commentService.getCommentsByPostId(postId);
    }

    @GetMapping("{commentId}")
    public ResponseEntity<CommentDto> getCommentById(
            @PathVariable("postId") Long postId,
            @PathVariable("commentId") Long commentId
    ) {
        CommentDto commentDto = commentService.getCommentById(postId, commentId);
        return new ResponseEntity<>(commentDto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<CommentDto> createComment(
            @PathVariable("postId") Long postId,
            @Valid @RequestBody CommentDto commentDto
    ) {
        return new ResponseEntity<>(
                commentService.createComment(postId, commentDto),
                HttpStatus.CREATED
        );
    }

    @PutMapping("{commentId}")
    public ResponseEntity<CommentDto> updateComment(
            @PathVariable("postId") Long postId,
            @PathVariable("commentId") Long commentId,
            @Valid @RequestBody CommentDto commentDto
    ) {
        CommentDto updateComment = commentService.updateComment(postId, commentId, commentDto);

        return new ResponseEntity<>(updateComment, HttpStatus.OK);
    }

    @DeleteMapping("{commentId}")
    public ResponseEntity<String> deleteComment(
            @PathVariable("postId") Long postId,
            @PathVariable("commentId") Long commentId
    ) {
        commentService.deleteCommentById(postId, commentId);
        return new ResponseEntity<>("Comment deleted successfully", HttpStatus.OK);
    }

}

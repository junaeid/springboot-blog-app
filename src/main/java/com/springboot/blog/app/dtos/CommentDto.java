package com.springboot.blog.app.dtos;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {
    private Long id;

    @NotEmpty(message = "Name Should not be null or empty")
    @NotNull
    private String name;

    @NotEmpty(message = "Email body should not be null or empty")
    @Email
    private String email;

    @NotEmpty
    @NotNull
    @Size(min = 10, message = "Comment should not be less than 10 characters")
    private String body;
}

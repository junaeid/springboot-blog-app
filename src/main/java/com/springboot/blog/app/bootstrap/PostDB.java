package com.springboot.blog.app.bootstrap;

import com.springboot.blog.app.dtos.PostDto;
import com.springboot.blog.app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class PostDB implements CommandLineRunner {

    private final PostService postService;

    @Autowired
    public PostDB(PostService postService) {
        this.postService = postService;
    }

    @Override
    public void run(String... args) throws Exception {
        if (postService.getAllPost(0, 10, "id", "asc").getContent().size() == 0) {
            loadDataToPost();
        }
    }

    private void loadDataToPost() {

        PostDto post1 = new PostDto();

        post1.setId(1L);
        post1.setTitle("Grimmia milleri Hastings & Greven");
        post1.setDescription("Hypnum recurvatum (Lindb. & Arnell) Kindb.");
        post1.setContent("Aufderhar and Sons");

        postService.createPost(post1);


        PostDto post2 = new PostDto();

        post2.setTitle("Nesoluma polynesicum (Hillebr.) Baill.");
        post2.setDescription("Populus ×acuminata Rydb. (pro sp.)");
        post2.setContent("Rath-Buckridge");

        postService.createPost(post2);

        PostDto post3 = new PostDto();
        post3.setTitle("Bryoria bicolor (Ehrh.) Brodo & D. Hawksw.");
        post3.setDescription("Solidago uliginosa Nutt. var. terrae-novae (Torr. & A. Gray) Fernald");
        post3.setContent("Davis Inc");

        postService.createPost(post3);

        PostDto post4 = new PostDto();
        post4.setTitle("Lupinus hyacinthinus Greene");
        post4.setDescription("Crocus vernus (L.) Hill");
        post4.setContent("D'Amore, VonRueden and Veum");

        postService.createPost(post4);

        PostDto post5 = new PostDto();
        post5.setTitle("Cirsium fontinale (Greene) Jeps.");
        post5.setDescription("Opuntia macrocentra Engelm.");
        post5.setContent("Heaney, Waelchi and Romaguera");

        postService.createPost(post5);

        PostDto post6 = new PostDto();
        post6.setTitle("Lespedeza procumbens Michx.");
        post6.setDescription("Carex gracillima Schwein");
        post6.setContent("Tromp Group");

        postService.createPost(post6);

        PostDto post7 = new PostDto();
        post7.setTitle("Carphephorus carnosus (Small) C.W. James");
        post7.setDescription("Opuntia macrocentra Engelm. var. macrocentra");
        post7.setContent("Grimes Group");

        postService.createPost(post7);


    }
}

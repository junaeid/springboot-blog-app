package com.springboot.blog.app.bootstrap;

import com.springboot.blog.app.models.Comment;
import com.springboot.blog.app.repositories.CommentRepository;
import com.springboot.blog.app.repositories.PostRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class CommentDB implements CommandLineRunner {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public CommentDB(PostRepository postRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        Comment comment1 = Comment.builder()
                .name("David Bird")
                .email("dbird0@surveymonkey.com")
                .body("projection")
                .post(postRepository.findById(1L).orElse(null))
                .build();

        commentRepository.save(comment1);

        Comment comment2 = Comment.builder()
                .name("Selena Elgey")
                .email("selgey1@nasa.gov")
                .body("Progressive")
                .post(postRepository.findById(1L).orElse(null))
                .build();
        commentRepository.save(comment2);

        Comment comment3 = Comment.builder()
                .name("Millicent Haresnaip")
                .email("mharesnaip2@photobucket.com")
                .body("uniform")
                .post(postRepository.findById(1L).orElse(null))
                .build();
        commentRepository.save(comment3);

        Comment comment4 = Comment.builder()
                .name("Whitaker Trowle")
                .email("wtrowle3@wired.com")
                .body("parallelism")
                .post(postRepository.findById(2L).orElse(null))
                .build();
        commentRepository.save(comment4);

        Comment comment5 = Comment.builder()
                .name("Jared Swanton")
                .email("jswanton4@parallels.com")
                .body("Managed")
                .post(postRepository.findById(2L).orElse(null))
                .build();
        commentRepository.save(comment5);
    }

}

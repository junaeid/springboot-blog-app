package com.springboot.blog.app.configs;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {
    //TODO: change to mapstruct
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}